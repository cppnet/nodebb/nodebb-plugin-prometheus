<div class="acp-page-container">
    <!-- IMPORT admin/partials/settings/header.tpl -->

    <div class="row m-0">
        <div id="spy-container" class="col-12 col-md-8 px-0 mb-4" tabindex="0">
            <form role="form" class="form-horizontal nodebb-plugin-prometheus-settings" id="nodebb-plugin-prometheus-settings">
                <div class="mb-3">
                    <label class="form-label" for="metricsAccessToken">[[nodebb-plugin-prometheus:metrics-access-token-label]]</label>
                    <div class="input-group">
                        <input class="form-control" type="text" id="metricsAccessToken" name="metricsAccessToken">
                        <button class="btn btn-outline-secondary" id="generateMetricsAccessToken">[[nodebb-plugin-prometheus:generate-metrics-access-token]]</button>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="metricsPath">[[nodebb-plugin-prometheus:metrics-path-label]]</label>
                    <input class="form-control" type="text" id="metricsPath" name="metricsPath" placeholder="/metrics">
                </div>
            </form>

            <div class="mt-4">
                <div class="col">
                    <p>
                        [[nodebb-plugin-prometheus:metrics-path-prefix]] <a id="metricsUrl" href=""></a>
                    </p>
                </div>
            </div>
        </div>

        <!-- IMPORT admin/partials/settings/toc.tpl -->
    </div>
</div>
