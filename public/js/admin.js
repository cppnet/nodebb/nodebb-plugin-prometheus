'use strict';

define('admin/plugins/prometheus', ['settings'], function(Settings) {
    let prometheus = {};

    function updateMetricsPath(settings) {
        settings = settings ? settings : Settings.helper.serializeForm($('#nodebb-plugin-prometheus-settings'));
        let url = config.relative_path + settings.metricsPath;
        if(settings.metricsAccessToken) {
            url += '?token=' + settings.metricsAccessToken;
        }

        $('#metricsUrl').attr('href', url).text(url);
    }

    prometheus.load = function() {
        Settings.load('nodebb-plugin-prometheus', $('#nodebb-plugin-prometheus-settings'), function (err, settings) {
            if(err) {
                return;
            }

            updateMetricsPath(settings);
        });
    };

    prometheus.init = function() {
        prometheus.load();

        $('#save').on('click', function() {
            Settings.save('nodebb-plugin-prometheus', $('#nodebb-plugin-prometheus-settings'), function () {
                updateMetricsPath();
                const saveBtn = document.getElementById('save');
                saveBtn.classList.toggle('saved', true);
                setTimeout(() => {
                    saveBtn.classList.toggle('saved', false);
                }, 1500);
            });
        });

        $('#generateMetricsAccessToken').on('click', function(e) {
            function uuidv4() {
                return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
                )
            }

            $('#metricsAccessToken').val(uuidv4());
            e.preventDefault();
        });

        $(window).on('action:reconnected', function () {
            if(window.location.pathname.startsWith(config.relative_path + '/admin/plugins/prometheus')) {
                prometheus.load();
            }
        });
    };

    return prometheus;
});
